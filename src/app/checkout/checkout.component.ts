import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Product } from '../products/product.model';
import { ProductsService } from '../services/products.service';
import { Subscription } from 'rxjs/Subscription';
import { CartIssuesService } from '../services/cart-issues.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  @ViewChild('f') checkOutForm: NgForm;
  cartProducts: Product[];
  submitted = false;
  cartTotal: number;
  cartAdditionSubscription: Subscription;
  cartTotalSubscription: Subscription;
  constructor(
    private prodService: ProductsService,
    private issue: CartIssuesService
  ) {}

  ngOnInit() {
    this.cartProducts = this.prodService.getCartAddedProducts();
    this.cartProducts = this.prodService.getCartAddedProducts();
    this.cartAdditionSubscription = this.prodService.cartAdditionEmitter.subscribe(
      (products: Product[]) => {
        this.cartProducts = products;
      }
    );

    this.cartTotal = this.prodService.getCartTotal();
    this.cartTotalSubscription = this.prodService.cartTotalEmitter.subscribe(
      (cTotal: number) => {
        this.cartTotal = cTotal;
      }
    );
  }

  onSubmit(form: NgForm) {
    this.submitted = true;

    const username = form.value.username;
    const email = form.value.email;
    const address = form.value.address;
    const phone = form.value.phone;
    const paymentType = form.value.paymentType;

    if (paymentType != '') {
      const val = {
        username,
        email,
        address,
        phone,
        paymentType
      };

      const test = this.cartProducts;
      test.forEach(e => {
        delete e.description;
        delete e.imagePath;
        delete e.imagePathSm;
      });
      const final = [];
      test.forEach(el => {
        final.push(Object.assign(el, val));
      });

      console.table(final);

      final.forEach(e => {
        this.issue.placeOrder(e).subscribe(data => {
          console.log(data);
        });
      });
      this.prodService.emptyCart();
      alert('Your Order has been Placed Successfuly');

      this.checkOutForm.reset();
    } else {
      alert('Enter Payment type Please');
    }
  }
}
