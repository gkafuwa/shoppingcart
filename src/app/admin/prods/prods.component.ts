import { Component, OnInit, ViewChild } from '@angular/core';
import { CartIssuesService } from '../../services/cart-issues.service';
import { NgForm } from '@angular/forms';
import { ToastyNotificationsService } from '../../services/toasty-notifications.service';

@Component({
  selector: 'app-prods',
  templateUrl: './prods.component.html',
  styleUrls: ['./prods.component.css']
})
export class ProdsComponent implements OnInit {
  @ViewChild('f') contactForm: NgForm;
  submitted = false;
  prods;
  constructor(
    private issue: CartIssuesService,
    private toastyNotifications: ToastyNotificationsService
  ) {}

  ngOnInit() {
    this.issue.getAll().subscribe(data => {
      this.prods = data;
    });
  }
  onSubmit(form: NgForm) {
    this.submitted = true;
    const name = form.value.name;
    // const email = form.value.email;
    // const message = form.value.message;
    // this.issue
    //   .update({
    //     name,
    //     email,
    //     message
    //   })
    //   .subscribe(data => {
    //     const message = data;
    //     this.toastyNotifications.addToast(false, JSON.stringify(message), true);
    //   });
    // this.contactForm.reset();
    console.log(name);
  }
}
