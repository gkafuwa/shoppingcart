import { Component, OnInit } from '@angular/core';
import { CartIssuesService } from '../../services/cart-issues.service';

@Component({
  selector: 'app-msg',
  templateUrl: './msg.component.html',
  styleUrls: ['./msg.component.css']
})
export class MsgComponent implements OnInit {
  messages;
  constructor(private sv: CartIssuesService) {}

  ngOnInit() {
    this.sv.get_contact().subscribe(data=>{
      this.messages = data;
    });
  }
}
