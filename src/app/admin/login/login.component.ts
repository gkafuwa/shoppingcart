import { Component, OnInit, ViewChild } from '@angular/core';
import { CartIssuesService } from '../../services/cart-issues.service';
import { ToastyNotificationsService } from '../../services/toasty-notifications.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('f') loginForm: NgForm;

  submitted = false;

  constructor(
    private issue: CartIssuesService,
    private toastyNotifications: ToastyNotificationsService,
    private rt: Router
  ) {}

  ngOnInit() {}

  onSubmit(form: NgForm) {
    this.submitted = true;
    const name = form.value.name;
    const pass = form.value.password;
    if (pass == '1234') {
      this.rt.navigate(['/admin/menu']);
    }
    this.loginForm.reset();
  }
}
