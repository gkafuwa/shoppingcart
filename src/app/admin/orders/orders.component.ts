import { Component, OnInit } from '@angular/core';
import { CartIssuesService } from '../../services/cart-issues.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders;
  constructor(private issue: CartIssuesService) {}

  ngOnInit() {
    this.issue.getOrder().subscribe(data => {
      this.orders = data;
    });
  }
}
