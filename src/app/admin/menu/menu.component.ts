import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  public sect = 'msg';
  constructor() {}

  ngOnInit() {}

  app(t) {
    this.sect = t;
  }
}
