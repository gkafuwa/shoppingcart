import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { CartIssuesService } from '../services/cart-issues.service';
import { ToastyNotificationsService } from '../services/toasty-notifications.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {
  @ViewChild('f') contactForm: NgForm;

  // to show data after submit
  submitted = false;

  constructor(
    private issue: CartIssuesService,
    private toastyNotifications: ToastyNotificationsService
  ) {}

  onSubmit(form: NgForm) {
    this.submitted = true;
    const name = form.value.name;
    const email = form.value.email;
    const message = form.value.message;
    this.issue
      .contact({
        name,
        email,
        message
      })
      .subscribe(data => {
        const message = data;
        this.toastyNotifications.addToast(false, JSON.stringify(message), true);
      });
    this.contactForm.reset();
  }
}
