import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CartIssuesService {
  public url = 'http://localhost/davidRest/';
  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/json')
  };

  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get(this.url, this.options);
  }
  postCart(cart) {
    this.http.post(this.url, cart, this.options);
  }
  update(cart) {
    this.http.put(this.url, cart, this.options);
  }
  contact(msg) {
    const domain = `${this.url}contact.php`;
    return this.http.post(domain, msg, this.options);
  }
  get_contact() {
    const domain = `${this.url}contact.php`;
    return this.http.get(domain, this.options);
  }

  placeOrder(msg) {
    const domain = `${this.url}orders.php`;
    return this.http.post(domain, msg, this.options);
  }
  getOrder() {
    const domain = `${this.url}orders.php`;
    return this.http.get(domain, this.options);
  }
 
}
