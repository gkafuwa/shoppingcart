import { TestBed, inject } from '@angular/core/testing';

import { CartIssuesService } from './cart-issues.service';

describe('CartIssuesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CartIssuesService]
    });
  });

  it('should be created', inject([CartIssuesService], (service: CartIssuesService) => {
    expect(service).toBeTruthy();
  }));
});
